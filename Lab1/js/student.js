$(document).ready(async function() {
    if ('serviceWorker' in navigator) {
      try {
        const reg = await navigator.serviceWorker.register('sw.js')
        console.log('Service worker register success', reg)
      } catch (e) {
        console.log('Service worker register fail')
      }
    }

    $.ajax({
        url: 'php/student-controller.php',
        type: "POST",
        data: JSON.stringify({action: "get"}),
        contentType: "application/json",
        dataType: "json",
        success: function(data){
            console.log(data);
            if(data.status){
                $('#hidden-input-id').val(Number(data.max_id) + 1);

                let elemsBody = document.querySelector("tbody");

                for (const stud of data.students) {
                    elemsBody.insertAdjacentHTML("beforeend",
                    `
                    <tr data-id=${stud.id}>
                        <th scope="row" style="text-align: center;"><input class="form-check-input" type="checkbox" value="" id="reverseCheck1"></th>
                        <td>${stud.studGroup}</td>
                        <td>${stud.firstName} ${stud.lastName}</td>
                        <td>${stud.gender === "Male" ? "M" : "F"}</td>
                        <td>${stud.birthday}</td>
                        <td><i class="fa-solid fa-circle stud-status"></i></td>
                        <td>
                            <button type="button" class="btn btn-outline-secondary btn-edit-stud" data-bs-toggle="modal" data-bs-target="#ModalAddEditStudent"><i class="fa-solid fa-pen"></i></button>
                            <button type="button" class="btn btn-outline-secondary btn-delete-stud"><i class="fa-solid fa-xmark"></i></button>
                        </td>
                    </tr>
                    `
                    )
                }
            } else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Error',
                    text: data.message,
                })
                return true;
            }
        },
        error: function(xhr, status, error) {
            console.log('Помилка при отриманні даних: ' + error);
        }
    });
})

let editElem;

class Student{
    constructor(id, group, firstName, lastName, gender, birthday) {
        this.id = Number(id);
        this.group = group;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthday = birthday;
    }
    getJSON(action, id){
        const json =  JSON.stringify(
        {
        "action": action,
        "id": id ? id : this.id,
        "group": this.group,
        "firstName": this.firstName,
        "lastName": this.lastName,
        "gender": this.gender,
        "birthday": this.birthday});
        return json;
    }
}

// Модальне вікна
var myModalAddEdit = new bootstrap.Modal($('#ModalAddEditStudent'), {
    keyboard: false
})

//Перехід до месенжера
$(".nav-link-notification").on("click", function(){
    //$("#notification-point").css("animation", "glowing 1200ms infinite");
    window.location.href = 'login.html';
})

let $btnAddEdit = $('button#btn-add-edit');
$btnAddEdit.on('click', function(event) {
    if(event.target.innerHTML === "Create")
    {
        AddStudent();
    } 
    else if(event.target.innerHTML === "Save")
    {
        EditStudent();
    }
})

function EditStudent(){
    
    const student = new Student($('#hidden-input-id').val(),$('#stud_group').val(),$('#stud_f_name').val(),$('#stud_l_name').val(),$('#stud_gender').val(),$('#stud_bday').val());
    $.ajax({
        url: 'php/student-controller.php',
        type: "POST",
        cache: false,
        data: student.getJSON("edit", editElem.parentElement.parentElement.getAttribute('data-id')),
        contentType: 'application/json',
        dataType: "json",
        success: function(data){
            if(data.status){
                console.log(data);
                const student = data.student;

                const arrChild = $(`tr[data-id='${student.id}']`)[0].children;
                arrChild[1].innerText = student.studGroup;
                arrChild[2].innerText = `${student.firstName} ${student.lastName}`;
                arrChild[3].innerText = student.gender === "Male" ? "M" : "F";
                arrChild[4].innerText = `${student.birthday}`;

                $('.select-group option')[0].selected = true;
                $('#stud_f_name').val('');
                $('#stud_l_name').val('');
                $('.select-gender option')[0].selected = true;
                $('#stud_bday').val('');

                myModalAddEdit.hide()
            } else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Error',
                    text: data.message,
                })
                return true;
            }
        }
    });
}

function AddStudent(){

        const student = new Student($('#hidden-input-id').val(),$('#stud_group').val(),$('#stud_f_name').val(),$('#stud_l_name').val(),$('#stud_gender').val(),$('#stud_bday').val());
        //console.log(student.getJSON);
        $.ajax({
            url: 'php/student-controller.php',
            type: "POST",
            cache: false,
            data: student.getJSON("add"),
            contentType: 'application/json',
            dataType: "json",
            
            success: function(data){
                console.log(data);
                if(data.status){
                    let elemsBody = document.querySelector("tbody");
                    elemsBody.insertAdjacentHTML("beforeend",
                    `
                    <tr data-id=${$('#hidden-input-id').val()}>
                        <th scope="row" style="text-align: center;"><input class="form-check-input" type="checkbox" value="" id="reverseCheck1"></th>
                        <td>${$('#stud_group').val()}</td>
                        <td>${$('#stud_f_name').val()} ${$('#stud_l_name').val()}</td>
                        <td>${$('#stud_gender').val()==="Male" ? "M" : "F"}</td>
                        <td>${$('#stud_bday').val()}</td>
                        <td><i class="fa-solid fa-circle stud-status"></i></td>
                        <td>
                            <button type="button" class="btn btn-outline-secondary btn-edit-stud" data-bs-toggle="modal" data-bs-target="#ModalAddEditStudent"><i class="fa-solid fa-pen"></i></button>
                            <button type="button" class="btn btn-outline-secondary btn-delete-stud"><i class="fa-solid fa-xmark"></i></button>
                        </td>
                    </tr>
                    `
                    )
                    $('#hidden-input-id').val(+$('#hidden-input-id').val() + 1);

                    $('.select-group option')[0].selected = true;
                    $('#stud_f_name').val('');
                    $('#stud_l_name').val('');
                    $('.select-gender option')[0].selected = true;
                    $('#stud_bday').val('');
                
                    myModalAddEdit.hide()
                } else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Error',
                        text: data.message,
                    })
                    return true;
                }
            }
        });
}

function RemoveStudent (event) {
    Swal.fire({
        title: 'Do you want to delete user?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Delete',
      }).then((result) => {
        if (result.isConfirmed) {

            let activeBtn = event.currentTarget;
            $.ajax({
                url: 'php/student-controller.php', // URL до PHP-скрипту
                type: 'DELETE', // метод запиту
                dataType: 'json', // формат відповіді
                contentType: 'application/json',
                data: JSON.stringify({"action": "delete","id": activeBtn.parentElement.parentElement.getAttribute('data-id')}), // дані в форматі JSON
                success: function(data) {
                    if(data.status){
                        console.log(data); // обробка відповіді
                        Swal.fire('Deleted!', '', 'success');
                        $(`tr[data-id='${data.id}']`).remove();
                        /* let activeBtn = event.currentTarget;
                        activeBtn.parentElement.parentElement.remove(); */
                    } else {
                        Swal.fire({
                            icon: 'warning',
                            title: 'Error',
                            text: data.message,
                        })
                        return true;
                    }
                },
            });
        }
      })

}
$(document).on('click', 'button.btn-delete-stud', RemoveStudent);

function ChangeStatus(event){
    let activeBtn = event.currentTarget;
    activeBtn.classList.toggle("online");
    if(activeBtn.classList.contains("online"))
        activeBtn.style.color = "green"; 
    else
        activeBtn.style.color = "#9b9b9b";

}
$(document).on('click', 'i.stud-status', ChangeStatus);

function FillingEditFields (event) {
    
    $("#ModalLabel").text("Edit student");
    $('button#btn-add-edit').text("Save");
    let activeBtn = event.currentTarget;
    editElem = event.currentTarget;
    console.log(activeBtn.parentElement.parentElement)
    const dataArr = activeBtn.parentElement.parentElement.children;

    let $groupOption = $('.select-group option')
    for (const option of  $groupOption) {
        //console.log(option)
        if(option.innerHTML === dataArr[1].innerHTML){
            option.selected = true;
            break;
        }
    }

    const firstLastName = dataArr[2].innerHTML.split(" ");

    $('#stud_f_name').val(firstLastName[0]);
    $('#stud_l_name').val(firstLastName[1]);

    let $genderOption = $('.select-gender option')
 
    if(dataArr[3].innerHTML === "M"){
        $genderOption[0].selected = true;
    }
    else{
        $genderOption[1].selected = true;
    }
    const dateStr = dataArr[4].innerHTML;

    const input = document.querySelector('input[type="date"]');
    input.value = dateStr;
}

$(document).on('click', 'button.btn-edit-stud', FillingEditFields);

function OpenModalToAdd (event) {
    $("#ModalLabel").text("Add student");
    $('button#btn-add-edit').text("Create");
}
$(document).on('click', 'button.btn-add-stud', OpenModalToAdd);