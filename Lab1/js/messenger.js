let user; // поточний користувач на сторінці
let anotherUsers =[]; // всі користувачі окрім поточного
let usersInChat = []; // користувачі які знаходяться в чаті окрім поточного користувача
let currentGroup; // поточний чат в якому знаходиться поточний користувач
let groups = []; // всі чати у яких знаходиться поточний користувач на сторінці

$(function(){
  var socket = io.connect();
  var $form = $("#message-form");
  var $input = $("#input-message");

  $form.submit(function(event){
    event.preventDefault();

    const newMessage = {
      groupId: currentGroup._id,
      sender: user.name,
      content: $input.val(),
      timestamp: new Date().toISOString().slice(0, 19).replace('T', ' '),
    }

    //надсилання повідомлення
    socket.emit('message', newMessage);
    allMessages = document.getElementById("all-mess");
    allMessages.insertAdjacentHTML("beforeend",
    `
    <div class="message-box">
      <div class="user-box">
        <img id="user-icon" src="./img/user.png" alt="Avatar" >
        <p>${newMessage.sender}</p>
      </div>
      <div class="message-content">
        <p class="message-time">${newMessage.timestamp}</p>
        <p>${newMessage.content}</p>
      </div>
    </div>
    `
    )
    $input.val("");
  });

  // Прийом повідомлень від сервера
  socket.on('message', (message) => {
    // Отримано нове повідомлення від сервера
    console.log('Отримано повідомлення:', message);

    if(message.groupId === currentGroup._id)
    {
      allMessages = document.getElementById("all-mess");
      allMessages.insertAdjacentHTML("beforeend",
      `
      <div class="message-box">
        <div class="user-box">
          <img id="user-icon" src="./img/user.png" alt="Avatar" >
          <p>${message.sender}</p>
        </div>
        <div class="message-content">
          <p class="message-time">${message.timestamp}</p>
          <p>${message.content}</p>
        </div>
      </div>
      `
      )
    }
    else
    {
      //дзвіночок відображає повідомлення якщо користувач знаходиться в іншій групі
      $("#notification-point").css("animation", "glowing 1200ms infinite");
      commingMessages = document.getElementById("coming-message");
      commingMessages.insertAdjacentHTML("beforeend",
      `
      <li>
        <div class="dropdown-item d-flex">
          <div class="d-flex flex-column align-items-center">
            <img id="user-icon" src="./img/user.png" alt="Avatar" >
              ${message.sender}
          </div>
          <div class="message-notification d-flex align-items-center">
            <p>${message.content}</p>
          </div>
        </div>
      </li>
      `
      )
    }
  });

  //ініціалізація месенжеру всіма даними
  axios.get('/getAllData')
    .then(response => {
      // Обробка успішної відповіді з сервера
      console.log(response);
      nameChatBox = document.getElementById("name-chats-box");
      usersConteiner = document.getElementById("users-container");
      chatName = document.getElementById("chat-name");
      allMessages = document.getElementById("all-mess");

      //отримую поточного користувача який увійшов на сторінку
      user = JSON.parse(sessionStorage.getItem('currentUser'));

      document.getElementById("user-name-signin").innerText = user.name;

      //фільтрую всі чати і отримую всі у яких є поточний користувач
      const userChats = response.data.filter(chat => {
        return chat.members.some(member => member.email === user.email);
      });

      currentGroup = userChats[0];

      //додавання назв чатів
      chatName.textContent = userChats[0].chatname;
      for(let i=0;i<userChats.length;i++)
      {
        groups.push(userChats[i]._id);
        //додавання до групи сокета
        socket.emit('joinGroup', userChats[i]._id);
        console.log("Юзер:  " + user.name +"  підключився до "+ userChats[i].chatname);
        //додавання назв чатів
        nameChatBox.insertAdjacentHTML("beforeend",
        `
        <div class="user-chat-box mb-2">
          <p>${userChats[i].chatname}</p>
        </div>
        `
        )
      }
      //додавання учасників
      userChats[0].members.forEach(member => {
        usersInChat.push(member);
        usersConteiner.insertAdjacentHTML("beforeend",
        `
        <div class="user-box">
          <img id="user-icon" src="./img/user.png" alt="Avatar" >
          <p style="margin-top: 5px; text-align: center;">${member.name}</p>
        </div>
        `
       )
      }); 
      //додавання кнопки "плюс"
      usersConteiner.insertAdjacentHTML("beforeend",
        `
        <div class="user-box" id = "btn-load-member" data-bs-toggle="modal" data-bs-target="#ModalAddMember">
          <img id="user-icon" src="./img/plus.png" alt="Avatar" >
          <p style="margin-top: 5px; text-align: center;">Додати</p>
        </div>
        `
        )

      //додавання попередніх повідомлень
      userChats[0].messages.forEach(message => {
        allMessages.insertAdjacentHTML("beforeend",
        `
        <div class="message-box">
          <div class="user-box">
          <img id="user-icon" src="./img/user.png" alt="Avatar" >
            <p>${message.sender}</p>
          </div>
          <div class="message-content">
            <p class="message-time">${message.timestamp}</p>
            <p>${message.content}</p>
          </div>
        </div>
        `
        )
      });
    })
    .catch(error => {
      // Обробка помилки
      console.error(error);
      Swal.fire({
          icon: 'error',
          title: 'Помилка',
          text: error.response.data,
      })
  });

  //загрузка користувачів після виклику модального вікна на додавання ЧАТУ
  function LoadUsers(name){
    axios.get('/loadUsers')
      .then(response => {

          let selectUsers = document.getElementById("select-users");
          selectUsers.innerHTML = "";

          //фільтрую щоб відобразити всіх коритувачів окрім поточного
          anotherUsers = response.data.filter(filterUser => filterUser.email !== user.email);
          console.log(anotherUsers);
          anotherUsers.forEach(user => {
            const option = document.createElement("option");
            option.text = `${user.name} (${user.email})`;
            selectUsers.appendChild(option);
          });
      })
      .catch(error => {
          // Обробка помилки
          console.error(error);
          Swal.fire({
              icon: 'error',
              title: 'Помилка',
              text: error.response.data,
          })
      });
  }
  $(document).on('click', '#btn-load-modal', LoadUsers);

  //загрузка користувачів після виклику модального вікна на додавання КОРИСТУВАЧІВ
  function LoadMembers(name){
    axios.get('/loadUsers')
      .then(response => {

          let selectUsers = document.getElementById("select-members");
          selectUsers.innerHTML = "";

          //фільтрую щоб відобразити всіх коритувачів яких немає в даному чаті
          anotherUsers = response.data.filter(filterUser => filterUser.email !== user.email && !usersInChat.some(user => user.email === filterUser.email));
          console.log(anotherUsers);
          anotherUsers.forEach(user => {
            const option = document.createElement("option");
            option.text = `${user.name} (${user.email})`;
            selectUsers.appendChild(option);
          });
      })
      .catch(error => {
          // Обробка помилки
          console.error(error);
          Swal.fire({
              icon: 'error',
              title: 'Помилка',
              text: error.response.data,
          })
      });
  }
  $(document).on('click', '#btn-load-member', LoadMembers);

  //створення нового чату
  function CreateChat(event){
    let chatName = document.getElementById("new-chat-name").value;

    if (chatName.trim() === '' || !/^[a-zA-Z\s]{3,}$/.test(chatName)) {
      // Неприпустиме значення
      Swal.fire({
        icon: 'error',
        title: 'Помилка',
        text: "Перевірне назву чату",
      })
      return;
    }

    let inviteMember = document.getElementById("select-users");

    //отримую емейли тих користувачів яких необхідно додати
    let selectedUsers = Array.from(inviteMember.selectedOptions).map(option => option.text.match(/\((.*?)\)/)[1]);
    let matchingUsers = anotherUsers.filter(user => selectedUsers.includes(user.email));
    matchingUsers.push(user);
    const chatData = {
      chatname : chatName,
      members : matchingUsers,
      messages : [],
    }

    axios.post('/createChat', chatData)
      .then(response => {
          // Обробка успішної відповіді з сервера
          console.log(response);
          location.reload();

      })
      .catch(error => {
          // Обробка помилки
          console.error(error);
          Swal.fire({
              icon: 'error',
              title: 'Помилка',
              text: error.response.data,
          })
      });
  }
  $(document).on('click', '#btn-create-chat', CreateChat);

  //лівнути з чату
  function LeaveChat(event) {
    const leaveUser = {
      email : user.email,
      chatID : currentGroup._id,
    } 
    axios.post('/leaveChat', leaveUser)
      .then(response => {
        // Обробка успішної відповіді з сервера
        console.log(response.data);
        location.reload();
        Swal.fire({
          icon: 'success',
          title: response.data,
        });
      })
      .catch(error => {
        // Обробка помилки
        console.error(error);
        Swal.fire({
          icon: 'error',
          title: 'Помилка',
          text: error.response.data,
        })
      });
  }
  $(document).on('click', '#btn-leave-chat', LeaveChat);


  //видалити чат
  function RemoveChat(event) {
    axios.post('/removeChat', {chatId: currentGroup._id})
      .then(response => {
        // Обробка успішної відповіді з сервера
        console.log(response.data);
        location.reload();
        Swal.fire({
          icon: 'success',
          title: response.data,
        });
      })
      .catch(error => {
        // Обробка помилки
        console.error(error);
        Swal.fire({
          icon: 'error',
          title: 'Помилка',
          text: error.response.data,
        })
      });
  }
  $(document).on('click', '#btn-remove-chat', RemoveChat);

//додавання нового учасника
  function AddNewMember(event) {

    //отримую емейли тих користувачів яких необхідно додати
    let inviteMember = document.getElementById("select-members");
    let selectedUsers = Array.from(inviteMember.selectedOptions).map(option => option.text.match(/\((.*?)\)/)[1]);
    let matchingUsers = anotherUsers.filter(user => selectedUsers.includes(user.email));

    const data = {
      chatID : currentGroup._id,
      members : matchingUsers,
    }

    axios.post('/addMembers', data)
      .then(response => {
          // Обробка успішної відповіді з сервера
          console.log(response);
          location.reload();

      })
      .catch(error => {
          // Обробка помилки
          console.error(error);
          Swal.fire({
              icon: 'error',
              title: 'Помилка',
              text: error.response.data,
          })
      });
  }
  $(document).on('click', '#btn-add-member', AddNewMember);

  //на натискання на дзвіночок пропадає анімація
  const bellButton = document.getElementById("bell");
    bellButton.addEventListener("click", function() {
      const point = bellButton.firstElementChild;
      if (point) {
        point.style.animation = "none";
      }
  });
});

//подія вибору іншого чату
function UpgradeChat(event){
  //отримую імя вибраної групи
  var selectChat = $(event.currentTarget).text().trim();

  axios.post('/getSelectChat', { name: selectChat })
    .then(response => {
      // Обробка успішної відповіді з сервера
      console.log(response);
      const chat = response.data;

      currentGroup = chat;
      console.log("Юзер:  " + user.name +"  перепідключився до поточної групи "+ currentGroup.chatname);

      nameChatBox = document.getElementById("name-chats-box");
      usersConteiner = document.getElementById("users-container");
      usersConteiner.innerHTML = "";
      chatName = document.getElementById("chat-name");
      allMessages = document.getElementById("all-mess");
      allMessages.innerHTML = "";


      chatName.textContent = chat.chatname;
      usersInChat = [];
      chat.members.forEach(member => {
        usersInChat.push(member);
        usersConteiner.insertAdjacentHTML("beforeend",
        `
        <div class="user-box">
          <img id="user-icon" src="./img/user.png" alt="Avatar" >
          <p style="margin-top: 5px; text-align: center;">${member.name}</p>
        </div>
        `
        )
      });
      usersConteiner.insertAdjacentHTML("beforeend",
      `
      <div class="user-box" id = "btn-load-member" data-bs-toggle="modal" data-bs-target="#ModalAddMember">
        <img id="user-icon" src="./img/plus.png" alt="Avatar" >
        <p style="margin-top: 5px; text-align: center;">Додати</p>
      </div>
      `
      )
      chat.messages.forEach(message => {
        allMessages.insertAdjacentHTML("beforeend",
        `
        <div class="message-box">
          <div class="user-box">
          <img id="user-icon" src="./img/user.png" alt="Avatar" >
            <p>${message.sender}</p>
          </div>
          <div class="message-content">
            <p class="message-time">${message.timestamp}</p>
            <p>${message.content}</p>
          </div>
        </div>
        `
        )
      });
    })
    .catch(error => {
      // Обробка помилки
      console.error(error);
      Swal.fire({
          icon: 'error',
          title: 'Помилка',
          text: error.response.data,
      })
  });
}
$(document).on('click', '.user-chat-box', UpgradeChat);