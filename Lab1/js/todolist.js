//let user; // поточний користувач на сторінці
//let currentGroup; // поточний чат в якому знаходиться поточний користувач
//let groups = []; // всі чати у яких знаходиться поточний користувач на сторінці

$(function(){
    let allTasks = []; // всі користувачі окрім поточного
    let todoTasks = []; // користувачі які знаходяться в чаті окрім поточного користувача
    let inProcessTasks = []; // користувачі які знаходяться в чаті окрім поточного користувача
    let doneTasks = []; // користувачі які знаходяться в чаті окрім поточного користувача
    let checkedTask;

    // Модальне вікна
    var myModalToDo = new bootstrap.Modal($('#ModalToDo'), {
      keyboard: false
    })
    var ModalInProcess = new bootstrap.Modal($('#ModalInProcess'), {
      keyboard: false
    })
    var ModalDone = new bootstrap.Modal($('#ModalDone'), {
      keyboard: false
    })
    var ShowModal = new bootstrap.Modal($('#myModal'), {
      keyboard: false
    })

    $(".nav-link-notification").on("click", function(){
      //$("#notification-point").css("animation", "glowing 1200ms infinite");
      window.location.href = 'login.html';
  })

  //ініціалізація todolist всіма даними
  axios.get('/getAllTasks')
    .then(response => {
      // Обробка успішної відповіді з сервера
      console.log(response);
      allTasks = response.data;
      //mainBox = document.getElementById("main-box");
      todoBox = document.getElementById("todo-box");
      inProcessBox = document.getElementById("in-process-box");
      doneBox = document.getElementById("done-box");
      

    // Проходимо по кожному об'єкту в масиві allTasks і розподіляємо їх відповідно до значення поля board
    allTasks.forEach(task => {
        if (task.board === "ToDo") {
            todoTasks.push(task);
        } else if (task.board === "In process") {
            inProcessTasks.push(task);
        } else if (task.board === "Done") {
            doneTasks.push(task);
        }
    });
    
    todoTasks.forEach(task => {
        const [date, time] = task.data.split(" ");
        todoBox.insertAdjacentHTML("beforeend",
        `
        <div class="task">
            <div class="d-flex justify-content-around">
                <div class="task-name me-1">
                    ${task.taskName} 
                </div>
                <div class="task-date">
                    <p class="task-date">
                        ${date}
                        <br>
                        ${time}
                    </p>
                </div>
            </div>
        </div>
        `
       )
      });
      inProcessTasks.forEach(task => {
        const [date, time] = task.data.split(" ");
        inProcessBox.insertAdjacentHTML("beforeend",
        `
        <div class="task">
            <div class="d-flex justify-content-around">
                <div class="task-name me-1">
                    ${task.taskName} 
                </div>
                <div class="task-date">
                    <p class="task-date">
                        ${date}
                        <br>
                        ${time}
                    </p>
                </div>
            </div>
        </div>
        `
       )
      }); 
      doneTasks.forEach(task => {
        const [date, time] = task.data.split(" ");
        doneBox.insertAdjacentHTML("beforeend",
        `
        <div class="task">
            <div class="d-flex justify-content-around">
                <div class="task-name me-1">
                    ${task.taskName} 
                </div>
                <div class="task-date">
                    <p class="task-date">
                        ${date}
                        <br>
                        ${time}
                    </p>
                </div>
            </div>
        </div>
        `
        )
      }); 
    })
    .catch(error => {
      // Обробка помилки
      console.error(error);
      Swal.fire({
          icon: 'error',
          title: 'Помилка',
          text: "Якась помилка",
      })
  });

  //загрузка користувачів після виклику модального вікна на додавання КОРИСТУВАЧІВ
  function AddTaskToDo(){

    let headerTask = document.getElementById("task-name").value;
    let contentTask = document.getElementById("task-content").value;

    let currentDate = new Date();
    let currentHour = currentDate.getHours();
    let newHour = currentHour + 3;
    currentDate.setHours(newHour);
    let DateTime = currentDate.toISOString().slice(0, 19).replace('T', ' ');

    const chatData = {
      board: "ToDo",
      taskName : headerTask,
      content : contentTask,
      data : DateTime,
    }

    axios.post('/addTaskToDo',chatData)
    
      .then(response => {
        todoBox = document.getElementById("todo-box");
        todoTasks.push(response.data);

        const [date, time] = response.data.data.split(" ");
        todoBox.insertAdjacentHTML("beforeend",
        `
        <div class="task">
            <div class="d-flex justify-content-around">
                <div class="task-name me-1">
                    ${response.data.taskName} 
                </div>
                <div class="task-date">
                    <p class="task-date">
                        ${date}
                        <br>
                        ${time}
                    </p>
                </div>
            </div>
        </div>
        `
        )
        myModalToDo.hide();
      })
      .catch(error => {
          // Обробка помилки
          console.error(error);
          Swal.fire({
              icon: 'error',
              title: 'Помилка',
              text: error.response.data,
          })
      });
  }
  $(document).on('click', '#btn-add-task-to-do-modal', AddTaskToDo);



  function LoadingInProcess(){
    let selectTasks = document.getElementById("select-tasks-in-process");
    selectTasks.innerHTML = "";

    todoTasks.forEach(task => {
      const option = document.createElement("option");
      option.text = `${task.taskName}`;
      selectTasks.appendChild(option);
    });
    
  }
  $(document).on('click', '#btn-add-task-process', LoadingInProcess);

  function AddTaskInProcess(){
    let selectasks = document.getElementById("select-tasks-in-process");
    let selectedTasksName = Array.from(selectasks.selectedOptions).map(option => option.text);
    const selectedTaskIds = todoTasks.filter(task => selectedTasksName.includes(task.taskName)).map(task => task._id);

    axios.post('/AddTaskInProcess', { taskIds: selectedTaskIds })
    
      .then(response => {
        Swal.fire({
          icon: 'success',
          title: 'Успішно',
          text: response,
        })
        ModalInProcess.hide();
        location.reload();
      })
      .catch(error => {
          // Обробка помилки
          console.error(error);
          Swal.fire({
              icon: 'error',
              title: 'Помилка',
              text: error.response.data,
          })
      });
    
  }
  $(document).on('click', '#btn-add-task-in-proc-modal', AddTaskInProcess);

  function LoadingDone(){
    let selectTasks = document.getElementById("select-tasks-done");
    selectTasks.innerHTML = "";

    inProcessTasks.forEach(task => {
      const option = document.createElement("option");
      option.text = `${task.taskName}`;
      selectTasks.appendChild(option);
    });
    
  }
  $(document).on('click', '#btn-add-task-done', LoadingDone);


  function AddTaskDone(){
    let selectasks = document.getElementById("select-tasks-done");
    let selectedTasksName = Array.from(selectasks.selectedOptions).map(option => option.text);
    const selectedTaskIds = inProcessTasks.filter(task => selectedTasksName.includes(task.taskName)).map(task => task._id);

    axios.post('/AddTaskDone', { taskIds: selectedTaskIds })
    
      .then(response => {
        Swal.fire({
          icon: 'success',
          title: 'Успішно',
          text: response,
        })
        ModalDone.hide();
        location.reload();
      })
      .catch(error => {
          // Обробка помилки
          console.error(error);
          Swal.fire({
              icon: 'error',
              title: 'Помилка',
              text: error.response.data,
          })
      });
    
  }
  $(document).on('click', '#btn-add-task-done-modal', AddTaskDone);

  //подія вибору іншого чату
function ShowTask(event){
  //отримую імя вибраної групи
  var selectChat = $(event.currentTarget).text().trim();
  var taskName = selectChat.substring(0, selectChat.indexOf('\n')).trim();

  const outputTask = allTasks.filter(task => task.taskName === taskName);
  checkedTask = outputTask[0];

  // Отримати посилання на елементи модального вікна за їх id
  const modalTitle = $("#modal-title");
  const modalContent = $("#modal-content");
  const modalDate = $("#modal-date");

  // Заповнити елементи модального вікна значеннями з об'єкта даних
  modalTitle.text(outputTask[0].taskName);
  modalContent.val(outputTask[0].content);
  modalDate.text(outputTask[0].data);

  // Відкрити модальне вікно
  $("#myModal").modal("show");
}
$(document).on('click', '.task', ShowTask);


  function RemoveTask(){
    const taskId = checkedTask._id;

    // Відправлення айді на сервер
    axios.post('/removeTask', { removeTaskId: taskId})
      .then(response => {
        // Видалення таска зі сторінки або оновлення списку тасків
        // залежно від вимог вашої програми
        Swal.fire({
          icon: 'success',
          title: 'Успішно',
          text: response.data,
        });
        ShowModal.hide();
        location.reload();
      })
      .catch(error => {
        console.error(error);
        Swal.fire({
          icon: 'error',
          title: 'Помилка',
          text: 'Помилка при видаленні таска',
        });
      });
  }
  $(document).on('click', '#remove-task', RemoveTask);


  function UpgradeTask(){

    const data = {
      content : $("#modal-content").val(),
      id : checkedTask._id,
    }

    axios.post('/upgradeTask', data)
      .then(response => {
        Swal.fire({
          icon: 'success',
          title: 'Успішно',
          text: response.data,
        });
        ShowModal.hide();
        location.reload();
      })
      .catch(error => {
        console.error(error);
        Swal.fire({
          icon: 'error',
          title: 'Помилка',
          text: 'Помилка при видаленні таска',
        });
      });
  }
  $(document).on('click', '#upgrade-task', UpgradeTask);

});






