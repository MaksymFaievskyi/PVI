
const inputsForm = document.querySelectorAll("input");
let signupBtn = document.getElementById("signupBtn");
let signinBtn = document.getElementById("signinBtn");
let nameDiv = document.getElementById("nameDiv");
let title = document.getElementById("title");
let nameField = document.getElementById("nameField");
let emailField = document.getElementById("emailField");
let passwordField = document.getElementById("passwordField");

function validateEmail(email) {
    const regExp = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    return regExp.test(email);
}

signupBtn.onclick = function(){
    let prevTitle = title.innerHTML;
    nameDiv.style.maxHeight = "60px";
    title.innerHTML = "Sign Up";
    signupBtn.classList.remove("disable")
    signinBtn.classList.add("disable")
    if(prevTitle === "Sign Up")
    {
        for (const field of inputsForm) {
            if(field.value === ""){
                Swal.fire({
                    icon: 'error',
                    title: 'Помилка',
                    text: "Заповніть всі поля!",
                })
                return;
            }
        }
        if(!validateEmail(emailField.value)) {
            Swal.fire({
                icon: 'error',
                title: 'Помилка',
                text: "Невірний емейл. Перевірте його!",
            })
            return;
        }
        if (String(passwordField.value).length < 8) {
            Swal.fire({
                icon: 'error',
                title: 'Помилка',
                text: "Пароль має містити щонайменше 8 символів. Перевірте його!",
            })
            return;
        }
        const newUser = {
            name : nameField.value,
            password : passwordField.value,
            email  : emailField.value,
        }

        axios.post('/singup', newUser)
            .then(response => {
                // Обробка успішної відповіді з сервера
                console.log(response.data);
                Swal.fire({
                    icon: 'success',
                    title: response.data,
                });
            })
            .catch(error => {
                // Обробка помилки
                console.error(error);
                Swal.fire({
                    icon: 'error',
                    title: 'Помилка',
                    text: error.response.data,
                })
        });
    }
}


signinBtn.onclick = function(){
    let prevTitle = title.innerHTML;
    nameDiv.style.maxHeight ="0px";
    title.innerHTML = "Sign In";
    signupBtn.classList.add("disable")
    signinBtn.classList.remove("disable")
    if(prevTitle === "Sign In")
    {
        for (const field of inputsForm) {
            if(field.name === "name") continue;
            if(field.value === ""){
                Swal.fire({
                    icon: 'error',
                    title: 'Помилка',
                    text: "Заповніть всі поля!",
                })
                return;
            }
        }
        if(!validateEmail(emailField.value)) {
            Swal.fire({
                icon: 'error',
                title: 'Помилка',
                text: "Невірний емейл. Перевірте його!",
            })
            return;
        }
        if (String(passwordField.value).length < 8) {
            Swal.fire({
                icon: 'error',
                title: 'Помилка',
                text: "Пароль має містити щонайменше 8 символів. Перевірте його!",
            })
            return; 
        }

        const newUser = {
            name : nameField.value,
            password : passwordField.value,
            email  : emailField.value,
        }
        console.log(newUser);
        axios.post('/signin', newUser)
        .then(response => {
            // Обробка успішної відповіді з сервера
            console.log(response.data);

            //ТУТ Я ДОДАЮ ДО sessionStorage ЩОБ ЗНАТИ ХТО УВІЙШОВ
            sessionStorage.setItem('currentUser', JSON.stringify(response.data));
            window.location = 'messenger.html';
        })
        .catch(error => {
            // Обробка помилки
            console.error(error);
            Swal.fire({
                icon: 'error',
                title: 'Помилка',
                text: error.response.data,
            })
        });
    }
}