const mongoose = require("mongoose");
const URL = "mongodb://127.0.0.1:27017/PVI_Labs"

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

server.listen(3000);

app.use(express.json());
app.use(express.static(__dirname));


mongoose.connect(URL,{})
    .then(result => console.log("database is connected"))
    .catch(err => console.log(err))


// Визначення моделі користувача
    const User = mongoose.model('User', {
    name: String,
    email: String,
    password: String,
});

// Визначення моделі чату
const Chat = mongoose.model('Chat', {
    chatname: String,
    members: [{
      name: String,
      email: String
    }],
    messages: [{
      sender: String,
      content: String,
      timestamp: String
    }]
  });

  // Визначення моделі завдання
  const Task = mongoose.model('Task', {
    board: String,
    taskName: String,
    content: String,
    data: String,
}); 

app.get('/',function(request,response) {
    response.sendFile(__dirname + '/index.html')
});

//Реєстрація
app.post('/singup', function(req, res) {
    const newUser = req.body;
    console.log(newUser);
    // Перевірка наявності користувача в базі даних
    User.findOne({ email: newUser.email })
        .then(existingUser => {
            if (existingUser) {
                // Користувач вже існує
                res.status(400).send('User already exists');
            } else {
            // Додавання нового користувача
                const user = new User(newUser);
                user.save()
                    .then(() => {
                        res.send('User created successfully');
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(500).send('Internal Server Error');
                    });
            }
        })
        .catch(err => {
        console.log(err);
        res.status(500).send('Internal Server Error');
        });
});

//Увійти
app.post('/signin', function(req, res) {
    const newUser = req.body;
    // Перевірка наявності користувача з введеним емейлом
    User.findOne({ email: newUser.email })
        .then(user => {
            if (!user) {
            // Користувача з таким логіном не знайдено
            return res.status(401).send('Incorrect login or password');
            }
            // Перевірка пароля
            if (user.password !== newUser.password) {
                // Неправильний пароль
                return res.status(401).send('Incorrect password!!!!');
            }
        // Логін і пароль співпадають
        res.send(user);
        })
        .catch(err => {
            // Помилка бази даних
            console.error(err);
            res.status(500).send('Internal Server Error');
        });
});

// Отримати дані про чати
app.get('/getAllData', function(req, res) {

    Chat.find({})
        .then(chats => {
            // Відправити успішну відповідь з даними про чати
            res.send(chats);
        })
        .catch(error => {
            // Обробка помилки
            console.error(error);
            res.status(500).send('Помилка сервера');
        });
});

app.get('/getAllTasks', function(req, res) {
    Task.find({})
        .then(tasks => {
            console.log(tasks);
            // Відправити успішну відповідь з даними про завдання
            res.send(tasks);
        })
        .catch(error => {
            // Обробка помилки
            console.error(error);
            res.status(500).send('Помилка сервера');
        });
});

app.post('/addTaskToDo', function(req, res) {
    const { board, taskName, content, data } = req.body;
    console.log(req.body);

    const task = new Task(req.body);
    task.save()
    .then(() => {
        res.send(req.body);
    })
    .catch(err => {
        console.log(err);
        res.status(500).send('Помилка сервера');
    });

});

app.post('/AddTaskInProcess', function(req, res) {
    const { taskIds } = req.body;
    
    Task.updateMany(
      { _id: { $in: taskIds } },
      { board: 'In process' }
    )
      .then(() => {
        res.send('Tasks updated successfully');
      })
      .catch(error => {
        console.error(error);
        res.status(500).send('Server error');
      });
  });

  app.post('/AddTaskDone', function(req, res) {
    const { taskIds } = req.body;
    
    Task.updateMany(
      { _id: { $in: taskIds } },
      { board: 'Done' }
    )
      .then(() => {
        res.send('Tasks updated successfully');
      })
      .catch(error => {
        console.error(error);
        res.status(500).send('Server error');
      });
  });

  app.post('/removeTask', function(req, res) {
    const TaskId = req.body.removeTaskId;
  
    // Виконання логіки видалення таска з бази даних за айді
    Task.findByIdAndRemove(TaskId)
      .then(() => {
        res.send('Таск видалено');
      })
      .catch(error => {
        console.error(error);
        res.status(500).send('Помилка сервера');
      });
  });

  app.post('/upgradeTask', function(req, res) {
    const { id, content } = req.body;
  
    // Знайти таск за айді
    Task.findByIdAndUpdate(id, { content: content }, { new: true })
      .then(updatedTask => {
        if (!updatedTask) {
          return res.status(404).send('Таск не знайдено');
        }
        res.send('Таск успішно оновлено');
      })
      .catch(err => {
        console.error(err);
        res.status(500).send('Помилка сервера');
      });
  });
  

//Отримання всіх користувачів
app.get('/loadUsers', function(req, res) {
    // Отримати всіх юзерів
    User.find({})
        .then(users => {
            // Відправити успішну відповідь з юзерами
            res.send(users);
        })
        .catch(error => {
            // Обробка помилки
            console.error(error);
            res.status(500).send('Помилка сервера');
        });
});

//Створення чату
app.post('/createChat', function(req, res) {
    const chatData = req.body;
  
    // Перевірка наявності чату з такою ж назвою
    Chat.findOne({ chatname: chatData.chatname })
      .then(existingChat => {
        if (existingChat) {
          // Чат з такою назвою вже існує
          res.status(400).send('Chat name already exists');
        } else {
          // Створення нового чату
          const chat = new Chat(chatData);
          chat.save()
            .then(() => {
              res.send(chat);
            })
            .catch(err => {
              console.log(err);
              res.status(500).send('Internal Server Error');
            });
        }
      })
      .catch(err => {
        console.log(err);
        res.status(500).send('Internal Server Error');
      });
});

//Отримати вибраний чат
app.post('/getSelectChat', function(req, res) {
    const { name } = req.body;
    Chat.findOne({ chatname: name })
        .then(chat => {
            if (chat) {
                res.send(chat);
            } else {
                res.status(404).send('Чат не знайдено');
            }
        })
        .catch(error => {
            // Обробка помилки
            console.error(error);
            res.status(500).send('Помилка сервера');
        }
    );
});

//Покинути чат
app.post('/leaveChat', function(req, res) {
    const leaveUser = req.body;

    // Знаходження чату за ID
    Chat.findById(leaveUser.chatID)
        .then(chat => {
            if (!chat) {
                return res.status(404).send('Chat not found');
            }
            // Пошук індексу учасника в чаті за email
            const memberIndex = chat.members.findIndex(member => member.email === leaveUser.email);

            if (memberIndex === -1) {
                return res.status(404).send('User not found in the chat');
            }

            // Видалення учасника з чату
            chat.members.splice(memberIndex, 1);

            // Збереження змін у базі даних
            return chat.save();
        })
        .then(savedChat => {
            // Успішна відповідь
            res.send('User left the chat successfully');
        })
        .catch(err => {
            console.error(err);
            res.status(500).send('Internal Server Error');
        });
});

//Видалити чат
app.post('/removeChat', function(req, res) {
    const chatId = req.body.chatId;

    // Знаходження чату за ідентифікатором
    Chat.deleteOne({ _id: chatId })
        .then(result => {
            if (result.deletedCount === 0) {
                // Чат не знайдено
                return res.status(404).send('Chat not found');
            }
            res.send('Chat deleted successfully');
        })
        .catch(err => {
            console.log(err);
            res.status(500).send('Internal Server Error');
        });
});

//Додавання учасників
app.post('/addMembers', function(req, res) {
    const { chatID, members } = req.body;

    // Знаходження чату за ідентифікатором
    Chat.findById(chatID)
        .then(chat => {
            if (!chat) {
                // Чат не знайдено
                return res.status(404).send('Chat not found');
            }
            // Додавання нових учасників до чату
            chat.members.push(...members);

            // Збереження змін у чаті
            chat.save()
                .then(() => {
                    res.send('Members added successfully');
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send('Internal Server Error');
                });
        })
        .catch(err => {
            console.log(err);
            res.status(500).send('Internal Server Error');
        });
});


io.sockets.on('connection', function (socket) {
    console.log("Підключились");

    // Обробник для отримання повідомлень від клієнта
    socket.on('message', (message) => {
        // Отримано нове повідомлення від клієнта
        console.log('Нове повідомлення:', message, message.groupId);
    
        // Створення нового повідомлення
        const newMessage = {
            groupId: message.groupId,
            sender: message.sender,
            content: message.content,
            timestamp: message.timestamp,
        };
    
        // Знаходження групи чату за ідентифікатором
        Chat.findById(message.groupId)
            .then((chat) => {
                // Додавання нового повідомлення до масиву повідомлень групи
                chat.messages.push(newMessage);
                // Збереження змін у базі даних
                return chat.save();
            })
            .then((savedChat) => {
                // Розсилка повідомлення всім учасникам групи
                socket.to(message.groupId).emit('message', newMessage);
            })
            .catch((err) => {
                console.error(err);
            });
    });

    // Приєднання клієнта до групи
    socket.on('joinGroup', (groupId) => {
        socket.join(groupId);
      });

    // Відєднання
    socket.on('disconnect', function(data) {
        console.log("Відключились");
    });
});