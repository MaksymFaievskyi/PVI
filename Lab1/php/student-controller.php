<?php
require_once 'student-model.php';
require_once 'student-model-dao.php';

$user = "root";
$password = "root";
$db = "student-bd";

try {
    $mysql = new mysqli('localhost', $user, $password);
    if ($mysql->connect_errno) {
        $mysql->close();
        throw new Exception("Failed to connect to MySQL: " . $mysql->connect_error);
    } else {
        $result = $mysql->query("SHOW DATABASES LIKE '$db'");
        if ($result === false || $result->num_rows === 0) {
            $mysql->close();
            throw new Exception('DB does not exist');
        } else {
            $mysql->select_db($db);
            $data = json_decode(file_get_contents('php://input'));

            $action = $data->action;
            $id = intval($data->id) ?? '';
            $group = $data->group ?? '';
            $firstName = trim($data->firstName) ?? '';
            $lastName = trim($data->lastName) ?? '';
            $gender = $data->gender ?? '';
            $birthday = $data->birthday ?? '';
        }
    }

}
catch (Exception $error) {
    $error = array('status' => false, 'message' => $error->getMessage());
    echo json_encode($error);
    exit();
}

$studentMySQL = new StudentDAO($mysql);

if ($action === 'get') {
    if($allStudents = $studentMySQL->getAllStudents())
    {
        $maxID = $studentMySQL->getMaxID();
        $studentsData = array();
        foreach ($allStudents as $student) {
            $studentsData[] = array(
                'id' => $student->getId(),
                'studGroup' => $student->getGroup(),
                'firstName' => $student->getFirstName(),
                'lastName' => $student->getLastName(),
                'gender' => $student->getGender(),
                'birthday' => $student->getBirthday()
            );
        }
        $data = array(
            'status' => true,
            'students' => $studentsData,
            'max_id' => $maxID
        );
        $studentMySQL->closeSQL();
        echo json_encode($data);
        exit();
    }
    else {
        $studentMySQL->closeSQL();
        $error = array('status' => false, 'message' => "There is no student in Database");
        echo json_encode($error);
        exit();
    }
}

if ($action === 'delete') {
    if ($studentMySQL->deleteStudent($id))
    {
        if ($studentMySQL->getSQL()->affected_rows > 0) {
            // Видалення пройшло успішно
            $response = array("status" => true, "id" => $id);
            http_response_code(200);
            // Повернення результату операції в json
            echo json_encode($response);
            $studentMySQL->closeSQL();
            exit();
        } else {
            $studentMySQL -> closeSQL();
            $error = array("status" => false, 'message' => 'Student with given id not found. Please, refresh page.');
            echo json_encode($error);
            exit();
        }
    } else {
        // Видалення не відбулося
        $error = array("status" => false, 'message' => $studentMySQL->getSQL()->error);
        $studentMySQL->closeSQL();
        echo json_encode($error);
        exit();
    }
}

$stud = new Student($id, $group, $firstName, $lastName, $gender, $birthday);
// перевіряємо дані на валідність
if(!$stud->validStudent())
{
    $error = $stud->getError();
    echo json_encode($error);
    exit();
}
if ($action === 'add') {
    if($studentMySQL->addStudent($stud->getId(), $stud->getGroup(), $stud->getFirstName(), $stud->getLastName(), $stud->getGender(),$stud->getBirthday()))
    {
        $studentMySQL->closeSQL();
        $response = array('status' => true);
        http_response_code(200);
        echo json_encode($response);
        exit();
    } else {
        $error = array('status' => false, 'message' => $studentMySQL->getSQL()->error);
        $studentMySQL->closeSQL();
        echo json_encode($error);
        exit();
    }
}

if ($action === 'edit') {
    if($studentMySQL->updateStudent($stud->getId(), $stud->getGroup(), $stud->getFirstName(), $stud->getLastName(), $stud->getGender(),$stud->getBirthday()))
    {
        if ($studentMySQL->getSQL()->affected_rows > 0) {
            $studentMySQL->closeSQL();
            $student = array(
            "id" => $id,
            "studGroup" => $group,
            "firstName" => $firstName,
            "lastName" => $lastName,
            "gender" => $gender,
            "birthday" => $birthday
            );

            $response = array(
            'status' => true,
            'student' => $student
            );
            http_response_code(200);
            echo json_encode($response);
            exit();
        } else {
            $studentMySQL->closeSQL();
            $error = array('status' => false, 'message' => 'Student with given id not found. Please, refresh page.');
            echo json_encode($error);
            exit();
        }
    } else {
        $error = array('status' => false, 'message' => $studentMySQL->getSQL()->error);
        $studentMySQL->closeSQL();
        echo json_encode($error);
        exit();
    } 
}   
?>