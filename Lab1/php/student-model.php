<?php

class Student {
  private $id;
  private $group;
  private $firstName;
  private $lastName;
  private $gender;
  private $birthday;
  private $error;

  // Конструктор класу
  public function __construct($id, $group, $firstName, $lastName, $gender, $birthday) {
    $this->id = $id;
    $this->group = $group;
    $this->firstName = $firstName;
    $this->lastName = $lastName;
    $this->gender = $gender;
    $this->birthday = $birthday;
  }

  // Геттери та сеттери для приватних полів класу
  public function getId() {
    return $this->id;
  }

  public function getGroup() {
    return $this->group;
  }

  public function setGroup($group) {
    $this->group = $group;
  }

  public function getFirstName() {
    return $this->firstName;
  }

  public function setFirstName($firstName) {
    $this->firstName = $firstName;
  }

  public function getLastName() {
    return $this->lastName;
  }

  public function setLastName($lastName) {
    $this->lastName = $lastName;
  }

  public function getGender() {
    return $this->gender;
  }

  public function setGender($gender) {
    $this->gender = $gender;
  }

  public function getBirthday() {
    return $this->birthday;
  }

  public function setBirthday($birthday) {
    $this->birthday = $birthday;
  }

  public function getError() {
    return $this->error;
  }

  public function validStudent() {
/*     if (empty($this->firstName)) {
      $this->error = array('status' => false, 'message' => 'You have not entered a first name. Please check!');
      return false;
    } */
/*     if(!preg_match('/^[A-Za-z]{3,30}$/', $this->firstName)){
      $this->error = array('status' => false, 'message' => 'You entered an incorrect first name. Please check!');
      return false;
    } */
    if (empty($this->lastName)) {
      $this->error = array('status' => false, 'message' => 'You have not entered a last name. Please check!');
      return false;
    }
    elseif(!preg_match('/^[A-Za-z]{3,30}$/', $this->lastName)){
      $this->error = array('status' => false, 'message' => 'You entered an incorrect last name. Please check!');
      return false;
    }
    elseif (empty($this->birthday)) {
      $this->error = array('status' => false, 'message' => 'You have not entered a birthday. Please check!');
      return false;
    }
    elseif (strtotime($this->birthday) > strtotime(date('Y-m-d'))) {
      $this->error = array('status' => false, 'message' => 'You entered an incorrect birthday. Please check!');
      return false;
    } else {
      return true;
    }
  }
}
?>