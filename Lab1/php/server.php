<?php
    header('Content-Type: application/json');

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      // отримуємо дані у форматі JSON
      $data = json_decode(file_get_contents('php://input'), true);

      $id = intval($data['id']); // може бути автоінкрементним
      $group = $data['group'];
      $firstName = $data['firstName'];
      $lastName = $data['lastName'];
      $gender = $data['gender'];
      $birthday = $data['birthday'];
      
      // перевіряємо дані на валідність
      $errors = array();
      if (empty($firstName)) {
        $error = array('status' => false, 'message' => 'You have not entered a first name. Please check!');
        echo json_encode($error);
        exit();
      }
      elseif(!preg_match('/^[A-Za-z]{3,30}$/', $firstName)){
        $error = array('status' => false, 'message' => 'You entered an incorrect first name. Please check!');
        echo json_encode($error);
        exit();
      }
      elseif (empty($lastName)) {
        $error = array('status' => false, 'message' => 'You have not entered a last name. Please check!');
        echo json_encode($error);
        exit();
      }
      elseif(!preg_match('/^[A-Za-z]{3,30}$/', $lastName)){
        $error = array('status' => false, 'message' => 'You entered an incorrect last name. Please check!');
        echo json_encode($error);
        exit();
      }
      elseif (empty($birthday)) {
        $error = array('status' => false, 'message' => 'You have not entered a birthday. Please check!');
        echo json_encode($error);
        exit();
      }
      elseif (strtotime($birthday) > strtotime(date('Y-m-d'))) {
          $error = array('status' => false, 'message' => 'You entered an incorrect birthday. Please check!');
          echo json_encode($error);
          exit();
      }
      else 
      {
        $mysql = new mysqli('localhost','root','root','student-bd');

        // Перевірка підключення
        if ($mysql->connect_error) {
          $error = array('status' => false, 'message' => 'Connection with Database failed!');
          echo json_encode($error);
          exit();
        }

        if($data['action'] == 'add') {

          // SQL запит для додавання нового запису
          $sql = "INSERT INTO `students` (`id`, `studGroup`, `firstName`, `lastName`, `gender`, `birthday`) 
                VALUES ('$id', '$group', '$firstName', '$lastName', '$gender', '$birthday')";

          // Виконання запиту
          if ($mysql->query($sql) === TRUE) {
            $mysql->close();
            $response = array(
              'status' => true,
            );
            http_response_code(200);
            echo json_encode($response);
          } else {
            $error = array('status' => false, 'message' => $mysql->error);
            $mysql->close();
            echo json_encode($error);
            exit();
          }
        } 
        else if($data['action'] == 'edit') {
          // SQL запит для редагування запису
          $sql = "UPDATE `students` SET `studGroup` = '$group', `firstName` = '$firstName', `lastName` = '$lastName', `gender` = '$gender', `birthday` = '$birthday' WHERE `id` = '$id'";
          $result = $mysql->query($sql);

          // Виконання запиту
          if ($result) {
            if ($mysql->affected_rows > 0) {
              $mysql->close();
              $student = array(
                "id" => $id,
                "studGroup" => $group,
                "firstName" => $firstName,
                "lastName" => $lastName,
                "gender" => $gender,
                "birthday" => $birthday
              );
    
              $response = array(
                'status' => true,
                'student' => $student
              );
              http_response_code(200);
              echo json_encode($response);
            } else {
              $mysql->close();
              $error = array('status' => false, 'message' => 'Student with given id not found. Please, refresh page.');
              echo json_encode($error);
              exit();
            }
          } else {
            $error = array('status' => false, 'message' => $mysql->error);
            $mysql->close();
            echo json_encode($error);
            exit();
          }
        }
      }
    } 
    elseif ($_SERVER['REQUEST_METHOD'] === 'GET') 
    {
      $mysqli = new mysqli('localhost','root','root','student-bd');

      // Перевірка підключення до бази даних
      if ($mysqli -> connect_error) {
          $error = array('status' => false, 'message' => 'Connection with Database failed!');
          echo json_encode($error);
          exit();
      }

      // Отримання всіх записів з таблиці
      $result = $mysqli->query("SELECT * FROM `students`");

      if ($result === FALSE) {
          $error = array('status' => false, 'message' => $mysqli->error);
          echo json_encode($error);
          exit();
      }

      // Ініціалізація масиву для збереження записів
      $records = array();

      // Отримання записів і збереження їх в масиві
      while ($row = $result->fetch_assoc()) {
          $records[] = $row;
      }

      // Отримання числа з найбільшим id
      $max_id = $mysqli->query("SELECT MAX(`id`) FROM `students`")->fetch_row()[0];

      // Формування масиву даних для відправки у форматі JSON
      $data = array(
          'status' => true,
          'students' => $records,
          'max_id' => $max_id
      );

      // Закриття з'єднання з базою даних
      $mysqli -> close();

      // Повернення даних у форматі JSON
      echo json_encode($data);
    } 
    elseif ($_SERVER['REQUEST_METHOD'] === 'DELETE') 
    {
      $id = json_decode(file_get_contents('php://input'))->id;

      $mysql = new mysqli('localhost','root','root','student-bd');

      // Перевірка підключення до бази даних
      if ($mysql -> connect_error) {
          $error = array('status' => false, 'message' => 'Connection with Database failed!');
          echo json_encode($error);
          exit();
      }

      // Видалення користувача з БД
      $sql = "DELETE FROM `students` WHERE `id` = '$id'";
      $result = $mysql->query($sql);

      // Перевірка результату операції
      if ($result) {
          if ($mysql->affected_rows > 0) {
              $mysql -> close();
              // Видалення пройшло успішно
              $response = array("status" => true, "id" => $id);
              http_response_code(200);
              // Повернення результату операції в json
              echo json_encode($response);
              exit();
          } else {
              $mysql -> close();
              $error = array("status" => false, 'message' => 'Student with given id not found. Please, refresh page.');
              echo json_encode($error);
              exit();
          }
          
      } else {
          // Видалення не відбулося
          $error = array("status" => false, 'message' => $mysql->error);
          $mysql -> close();
          echo json_encode($error);
          exit();
      }
    }
?>