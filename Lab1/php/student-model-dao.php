<?php
require_once 'student-model.php';
class StudentDAO {
  private $mysql;

  // Конструктор класу
  public function __construct($mysql) {
    $this->mysql = $mysql;
  }

  public function getSQL() {
    return $this->mysql;
  }

  // Метод для отримання списку завдань
  public function getAllStudents() {
    $stmt = $this->mysql->query("SELECT * FROM `students`");

    if ($stmt->num_rows == 0) {
      return false;
    } else {
      $students = array();
      while ($row = $stmt->fetch_assoc()) {
        $stud = new Student($row['id'], $row['studGroup'], $row['firstName'], $row['lastName'], $row['gender'], $row['birthday']);
        $students[] = $stud; 
      }
      return $students;
    }
  }

  // Метод для отримання списку завдань
  public function getMaxID() {
    $result = $this->mysql->query("SELECT MAX(`id`) FROM `students`")->fetch_row()[0];

    return $result;
  }

  public function closeSQL() {
    $this->mysql->close();
  }

  // Метод для додавання нового завдання
  public function addStudent($id, $group, $firstName, $lastName, $gender, $birthday) {
    $stmt = $this->mysql->query("INSERT INTO `students` (`id`, `studGroup`, `firstName`, `lastName`, `gender`, `birthday`) 
      VALUES ('$id', '$group', '$firstName', '$lastName', '$gender', '$birthday')");
    return $stmt === TRUE ? true : false;
  }

  // Метод для видалення завдання за id
  public function deleteStudent($id) {
    $stmt = $this->mysql->query("DELETE FROM `students` WHERE `id` = '$id'");
    return $stmt === TRUE ? true : false;
  }

  // Метод для оновлення статусу завдання за id
  public function updateStudent($id, $group, $firstName, $lastName, $gender, $birthday) {
    $stmt = $this->mysql->query("UPDATE `students` SET `studGroup` = '$group', `firstName` = '$firstName', `lastName` = '$lastName', `gender` = '$gender', `birthday` = '$birthday' WHERE `id` = '$id'");
    return $stmt === TRUE ? true : false;
  }
}
?>